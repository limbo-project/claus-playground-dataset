#!/bin/bash
source config.sh


mimes=()
echo "$DATASET_FILES" | while read file; do
  mime=`xdg-mime query filetype "$file"`
 echo "$file $mime"
  mimes+=("$mime")


done

DATASET_MIMES=$( IFS=$'\n'; echo "${mimes[*]}" )

#echo "${DATASET_MIMES[@]}"

export DATASET_MIMES
