# claus-playground-dataset

tagging the git repo on release
there should be a mechanism to release multiple dataset at once - at least multiple ones defined in a single dcat file

one convention is to simply check how many different versions there are for each dataset, and if its only one, then we can use this as a tag



If we allowed JSON-LD, we could also use JavaScript in addition to SPARQL for transforming the DCAT models:
resources.filter(_.type = 'dataset').foreach(x -> x[@id] = x[@id] || 'some Default')


Repo for me (Claus) to play around


The goals of the git repositories hosting datasets (henceforth referred to as dataset repositories) is to have a well defined procedure for:

* generation+validation of dcat metadata (mandatory)
* generation+validation of data (optional)


The obtained DCAT metadata record can then be deployed to a dataset catalog.


In essence, only an RDF file containing DCAT metadata is needed. Data 



## What is a dataset?


A dataset is an instance of a (possibly informal) schema = data model = language.



* Eventually, every dataset needs to be represented as a sequence of bytes, i.e. a file.
* Every file should be treated as a distribution of one or more datasets.
* A collection of RDF datasets is a form of distribution of their RDFUnionDataset.




Datasets should be identified by equivalence of their content. Eventutally, a dataset must be represented as a file, i.e. a sequences of bytes.


Every file with relevant data should be considered a distribution of a dataset. 


The main point I want to make here is: DO NOT think of a dataset as a bundle of arbitrary resources. You will find many CKAN records which claim that loosly related APIs, PDFs (data), PDFs(manuals / notes), RDF and CSV files are all "distributions" of the "same dataset". An aspect of the Semantic Web is to devise "good" models that ease the creation of smart-acting agents.

The primary conceptual contribution of DCAT was the introduction of the distinction between a dataset and its distributions.

This principle is common in software engineering with every dependency management tool: No serious developer will claim that depending on files directly is a good idea. Instead, the best practice is to use artifact identifiers (apt, maven, NPM, pip, etc) which are resolved against a configurable list of repositories.
Now, an artifact is technically an archive - and thus is a collection of items (files), which may happen to be datasets.






This indirection has a very useful property: When you e.g. download a file distribution of dataset, you have not created a new dataset, but you have created your own local distribution of it. Thi


- family of datasets.
People often say that they used DBpedia data in their project. However, 







